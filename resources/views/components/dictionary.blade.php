 <script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
 
<div id="dictionary-carousel" class="carousel slide " data-ride="carousel" data-interval="5000">
    <div class="carousel-inner">
        <!-- DICTIONARY 1 -->
        <div class="carousel-item active">
            @component('components.dictionaries')
                @slot('title')
                {{ __('home.translation') }}
                @endslot
                @slot('grammar')
                {{ __('home.translationGrammar') }}
                @endslot
                @slot('def')
                {{ __('home.translationDef') }}
                @endslot
                @slot('img')
                /imgs/interpretes-home.png
                @endslot
            @endcomponent
         </div>
    <div class="carousel-item">
        <!-- DICTIONARY 2 -->
        @component('components.dictionaries')
                @slot('title')
                {{ __('home.interpretation') }}
                @endslot
                @slot('grammar')
                {{ __('home.interpretationGrammar') }}
                @endslot
                @slot('def')
                {{ __('home.interpretationDef') }}
                @endslot
                @slot('img')
                /imgs/interpretes-home.png
                @endslot
        @endcomponent     
    </div>
  </div>
</div>