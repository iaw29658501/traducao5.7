
<?php
return [
    'title' => 'I+D',
    'subtitle' => 'Investigación y Desarrollo',
    'body' => 'Coincidiendo con la celebración del 10º aniversario HUMANA inaugura su nueva sede en el Parque Tecnológico del Guamá, en la Universidad Federal del Pará, PCT Guamá. En este espacio se pretende desarrollar nuevos productos de base tecnológica, en colaboración con empresas locales, para atender el mercado de la traducción e interpretación.'
];
