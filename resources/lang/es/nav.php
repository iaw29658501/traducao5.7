<?php

return [
    'home' => '
    Inicio',
    'company' => 'La Empresa',
    'presentation' => 'Presentación',
    'companyCore' => 'Las bases de nuestra empresa',
    'interpretation' => 'Interpretación',
    'versatility' => 'Versatilidad',
    'mobility' =>'Movilidad',
    'bigevents' =>'Eventos Emblemáticos',
    'comunicativeSituation' => 'Situación comunicativa',
    'translation' => 'Traduciones',
    'portfolio' => 'Portafolio de clientes y servicios',
    'tranlationAndNewTech' => 'Traducción hoy y nuevas tecnologías',
    'qualityAndProcedures' => 'Control de calidad y procesos.',
    '10years' => '10 años',
    'humanInstitute' => 'Instituto Humana',
    'idc' => 'ID+C (PCT GUAMÁ/Brasil)',
    'country' => 'es',
    'language' => 'Español',
    'contactus' => 'Contáctenos'
];

