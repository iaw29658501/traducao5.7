<?php

return [
    'title' => 'PROCESOS Y CONTROL DE CALIDAD',
    'subtitle' => 'Atención a los detalles',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'Una vez recibido el texto, para garantizar la calidad a lo largo de toda la gestión del proyecto y hasta la traducción final, se establece un sistema de diferentes niveles que sigue los siguientes pasos:
        <br><br>
        • Selección del equipo de traducción.
<br><br>
        • Flujo de trabajo donde se garantiza un sistema de verificación cruzada con revisión por pares.
<br><br>
<br>
        • Uso de la tecnología más actual para la gestión de contenido, gestión de la documentación y elaboración de informes.
<br><br>
<br>
        • Sistema de gestión del proyecto que garantiza que todas las demandas del cliente sean atendidas, permitiendo, al mismo tiempo, una respuesta rápida a las solicitudes de última hora o los cambios no detectados.
        <br><br>
        Más concretamente, los Principios y Metodología de Gestión de Calidad de Traducción que la empresa adopta son los siguientes:
        <br>
        • Sobre la selección del traductor se atienden las siguientes cuestiones: ¿Qué calificaciones debe tener quien reciba el encargo de traducir?  ¿Qué experiencia tiene en traducción o en el área específica del encargo? ¿Ha traducido en los últimos años?
        De esta forma, la configuración del equipo de traducción se hace de acuerdo con las necesidades específicas del proyecto y representa el primer paso hacia una Gestión del Calidad de Traducción eficaz y consistente.
        • En relación a las tecnologías empleadas, se establece la herramienta TAO (Traducción Asistida por Ordenador) de acuerdo con las especificaciones del proyecto, número de participantes y sistema de trabajo.  En general la empresa utiliza las tecnologías más avanzadas en Memorias de Traducción y Gestión Terminológica, como son memoQ o la plataforma para trabajos en red Smartcat. En todos los casos los programas permiten que las mismas frases sean traducidas de forma idéntica y la terminología se utilice consistentemente a lo largo del proyecto, aunque distintos traductores trabajen en partes diferentes del mismo texto.
<br><br>
        • Sobre el proceso de edición, para minimizar la posibilidad de errores u omisiones en la información o el lenguaje, el mismo texto pasa por las manos de un traductor y un revisor que son traductores profesionales, nativos en la lengua de llegada, y que tienen las calificaciones específicas necesarias para el proyecto.
<br><br>
        • Con relación a la Gestión del Proyecto de Traducción se siguen las siguientes pautas:
            ◦ adecuación del flujo del proceso de traducción a las necesidades específicas del cliente.
<br><br>
            ◦ preparación y seguimiento de listas de control de calidad.
<br><br>
            ◦ comunicación proactiva con el cliente para solucionar cuestiones lingüísticas o técnicas.
<br><br>
            ◦ mantenimiento de glosarios y memorias de traducción multilingües específicas para cada proyecto.
<br><br>
            ◦ acopio de documentación individualizada del historial del proyecto.',
    ];
