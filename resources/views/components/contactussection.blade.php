<!-- Section: Contact v.1 -->
<style>
    .map-container-section {
        overflow: hidden;
        padding-bottom: 56.25%;
        position: relative;
        height: 0;
    }

    .map-container-section iframe {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
    }
</style>
<section class="my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold text-center my-5">{{ __('contact.contactus') }}</h2>
    <!-- Section description -->
    {{-- <p class="text-center w-responsive mx-auto pb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam
      eum porro a pariatur veniam.</p> --}}

    <!-- Grid row -->
    <div class="row">

        <!-- Grid column -->
        <div class="col-lg-5 mb-lg-0 mb-4">

            <!-- Form with header -->
            <form method="POST" action="/contactme">
                <div class="card">
                    <div class="card-body">
                        @csrf
                        <!-- Body -->
                        <div class="md-form">
                            <i class="fas fa-user prefix grey-text"></i>
                            <label for="name">{{ __('contact.yourname') }}</label>
                            <input name="name" type="text" id="name" class="form-control" required>
                        </div>
                        <div class="md-form">
                            <i class="fas fa-envelope prefix grey-text"></i>
                            <label for="email">{{ __('contact.youremail') }}</label>
                            <input name="email" type="text" id="email" class="form-control" required>
                        </div>
                        <div class="md-form">
                            <i class="fas fa-tag prefix grey-text"></i>
                            <label for="subject">{{ __('contact.subject') }}</label>
                            <input name="subject" type="text" id="subject" class="form-control" required>
                        </div>
                        <div class="md-form">
                            <i class="fas fa-pencil-alt prefix grey-text"></i>
                            <label for="body">{{ __('contact.message') }}</label>
                            <textarea name="body" id="body" class="form-control md-textarea" rows="3"
                                required></textarea>
                        </div>
                        <div class="md-form">
                            <div class="g-recaptcha mt-2" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                            @if($errors->has('g-recaptcha-response'))
                            <span class="invalid-feedback" style="display: block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="md-form" id="gdprdiv">
                            <label for="gdpr"></label>
                            <br>
                            <input type="checkbox" class="form-check-input" id="gdpr" name="gdpr">
                        </div>
                        <div class="text-center">
                            <br>
                            <button class="btn btn-success">{{ __('contact.send') }}</button>
                        </div>
                    </div>
                </div>
            </form>
            <script>
                {{--  hide checkbox input used to detect bots and spam --}}
            function loaded() {
                var _0x4750=['hide'];(function(_0xbcfe22,_0x475099){var _0x46b07f=function(_0x2b9257){while(--_0x2b9257){_0xbcfe22['push'](_0xbcfe22['shift']());}};_0x46b07f(++_0x475099);}(_0x4750,0x95));var _0x46b0=function(_0xbcfe22,_0x475099){_0xbcfe22=_0xbcfe22-0x0;var _0x46b07f=_0x4750[_0xbcfe22];return _0x46b07f;};$('#gdprdiv')[_0x46b0('0x0')]();
            }
            window.addEventListener ?
            window.addEventListener("load",loaded,false)
            :
            window.attachEvent && window.attachEvent("loaded",resize);
            </script>
            <!-- Form with header -->

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-7">

            <!--Google map-->
            <div id="map-container-section" class="z-depth-1-half map-container-section mb-4" style="height: 400px">
                <iframe
                    src="https://maps.google.com/maps?q=Espaço+Empreendedor+Belem&t=&z=15&ie=UTF8&iwloc=&output=embed"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- Buttons-->
            <div class="row text-center">
                <div class="col-md-4">
                    <a class="btn-floating blue accent-1">
                        <i class="fas fa-map-marker-alt"></i>
                    </a>
                    <p> Espaço Empreendedor, PCT Guamá,
                        Av. Perimetral da Ciência
                        Km 01, módulo 308
                        CEP 66055-110
                        Belém – PA</p>
                    <p class="mb-md-0">Brasil</p>
                </div>
                <div class="col-md-4">
                    <a class="btn-floating blue accent-1">
                        <i class="fas fa-phone"></i>
                    </a>
                    <p>+ (91) 9 8015-3485</p>
                </div>
                <div class="col-md-4">
                    <a class="btn-floating blue accent-1">
                        <i class="fas fa-envelope"></i>
                    </a>
                    <a href="mailto:humana.com.trad@gmail.com">
                        <p class="mb-0">humana.com.trad@gmail.com</p>
                    </a>
                </div>
            </div>

        </div>
        <!-- Grid column -->

    </div>
    <!-- Grid row -->

</section>
<!-- Section: Contact v.1 -->
