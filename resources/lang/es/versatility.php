<?php

return [
    'title' => 'VERSATILIDAD',
    'subtitle' => 'Adaptación',
    'alt' => 'Traducción Brasil',
    'body' => 'Durante los primeros diez años de la trayectoria de HUMANA COM & TRAD, los 233 servicios de interpretación prestados por la compañía han sido en eventos de diversas disciplinas académicas y temas.  Cabe destacar los congresos, reuniones y coloquios sobre asuntos de ámbito político, jurídico o económico (87), medio ambiente (48) y medicina. La versatilidad a la hora de tratar temas variados viene acompañada de una capacidad de adaptación a la hora de trabajar en lugares distantes, todo lo cual representa valores añadidos que la empresa potencia al máximo.  HUMANA COM & TRAD es una empresa con la que se puede contar en el momento de montar infraestructura compleja y que dispone tanto de recursos humanos como materiales para atender las más variadas situaciones comunicativas para lugares con grandes desafíos logísticos.'
];
