@component('components.template')
@slot('nav')
blue
@endslot
@slot('navdontchange')
dont
@endslot
@slot('image')
/imgs/nice/eventos-emblematicos.jpg
@endslot
@slot('logo')
/imgs/eventos.png
@endslot
@slot('alt')
Realizamos tradução simultanea para grandes eventos
@endslot
@slot('title')
{{ __('bigevents.title')  }}
@endslot
@slot('subtitle')
{{ __('bigevents.subtitle')  }}
@endslot
@slot('body')
{!! __('bigevents.body') !!}
@endslot
@slot('map')
'yes'
@endslot
@endcomponent
