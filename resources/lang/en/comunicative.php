<?php

return [
    'title' => 'COMMUNICATIVE SITUATION',
    'subtitle' => 'Specificities of the events',
    'alt' => 'Serviço de tradução simultanea em todo Brasil',
    'body' => 'To meet the demand for specialized services, HUMANA COM & TRAD analyzes the particular conditions of each event to decide on the human and technical resources needed to render the best in the area of interpretation. Headquartered near the northern borders of Brazil the company has surmounted additional difficulties both in logistics and in the qualified human resources available. To overcome these special conditions, HUMANA has had, over the years, a series of adaptation mechanisms to the environment, with specially designed travel equipment, easy and quick to assemble, and a highly qualified and willing human team in which can work even in extreme conditions, both in the urban and forest environments regardless of distances and terrain to be covered.
    In addition, since its inception, the company has organized a series of training courses for young translators and interpreters, conducted by the HUMANA Institute in collaboration with local university centers and other institutions all over Brazil. This is the reason for HUMANA´s training activities being fundamental in transforming the landscape of shortage of trained resources into a conducive environment where major massive events such as the World Social Forum and IDEA 2010 have been executed to perfection.'
];
