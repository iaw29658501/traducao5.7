@component('components.template')
@slot('image')
/imgs/nice/situacao-comunicativa-traducao-em-belem.jpg
@endslot
@slot('logo')
/imgs/situacao.png
@endslot
@slot('alt')
{{ __('comunicative.alt')  }}
@endslot
@slot('title')
{{ __('comunicative.title')  }}
@endslot
@slot('subtitle')
{{ __('comunicative.subtitle')  }}
@endslot
@slot('body')
{{ __('comunicative.body')  }}
@endslot
@endcomponent
