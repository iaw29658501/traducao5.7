<?php

namespace App\Http\Controllers;

use App\Contactme;
use Illuminate\Http\Request;
use App\Rules\Captcha;

class ContactController extends Controller
{
    function index() {
        return view('contactme', [ 'contactme' => Contactme::orderBy('id','DESC')->get()]);
    }

    function store(Request $request) {
        $this->validate($request, [
          //  'g-recaptcha-response'=> new Captcha(),
            'body' => [ 'not_regex:/^.*(sex|sexy|sexo|dick|pussy|fuck|virginity|suck|porn|telegram|cachonda|cachondo).*$/i' ],
            'email' => [ 'email','not_regex:/^.*(noreply|no\-reply).*$/i','email_checker' ],
        ]);
        if (! $request->gdpr) {
            $contactme = new Contactme($request->all());
            $contactme->save();
        }
        return redirect('/home');
    }

    function delete($contact) {
        $contact = Contactme::findOrFail($contact);
        $contact->delete();
        return redirect('/contactme');
    }
}
