<?php

return [
    'title' => 'MOBILIDADE',
    'subtitle' => 'Adaptação ao meio',
    'body' => 'A estrutura e a organização da HUMANA COM & TRAD permitem que a empresa possua grande agilidade assim como mobilidade geográfica. No Brasil, ela tem atendido à procura por serviços de interpretação em diversas localidades afastadas da sua sede em Belém do Pará, como foi o caso de Macapá, Porto Velho, Porto Alegre ou Maceió. Isso fez com que ela cobrisse o país de norte a sul e de leste a oeste.'
];
