<div class="container-fluid m-0 p-0 img-text-section">
    <div class="row no-gutters">
        <div class="col-sm-1-12 col-md-6 m-0 p-0 h-100">
            <div id="parallaxtest2"></div>
        </div>
        <div class="col-sm-1-12 col-md-6 align-items-center"
            style="background-color: #1e50f7">
            <div class="container p-3  h-100 d-flex align-items-start flex-column justify-content-center">
                <h1 class="text-light"><b>{{ __('home.vision')  }} </b></h1>
                <h3 class="text-light medium">{{ __('home.visionText') }} </h3>
            </div>
        </div>
    </div>
</div>
