<?php

return [
    'title' => 'EL FUTURO DE LA EMPRESA',
    'subtitle' => 'INSTITUTO HUMANA, nuevas perspectivas',
    'alt' => 'Traducción humana',
    'body' => 'En este apartado se tratan las actividades de difusión del conocimiento y las relativas al mundo de la interpretación simultánea y la traducción, así como temas de interés dentro de la cultura humanística en general. El Instituto HUMANA recoge, da continuidad y amplía las actividades llevadas a cabo por la empresa HUMANA. El Instituto cuenta con un historial de cursos de formación de nuevos intérpretes desde 2009 hasta la actualidad. La mayoría de sus participantes han pasado por un período de prácticas e iniciado su carrera dentro de la empresa. Además, el Instituto cuenta con recursos didácticos para la enseñanza de lenguas y técnicas de formación de traductores e intérpretes de conferencia.
    <br><br>
    [RECURSOS DIDÁCTICOS: banco de datos de libros y archivos digitales, por temas]
    <br>
[Cuestionario para los participantes de los cursos]
<br>
<br>
Se recogen igualmente aquí las actividades de formación continua atendidas por su director, Sandro Ruggeri Dulcet, traductor e intérprete profesional autónomo desde 1995, año en que empieza a trabajar atendiendo a diferentes clientes y empresas locales del sector. Podemos destacar la participación de Humana en congresos: ABRATES (2007), como poniente sobre el estado de la traducción y la interpretación en la región Norte de Brasil; SIMB! (2013); y ELIA TOGETHER (2016), como participante. O bien los cursos a distancia, entre los que destacamos las especializaciones en Traducción Inglés-Español por la UNED, o Traducción Español-Portugués por la Gama Filho-Estácio de Sá; y los mini cursos sobre temáticas específicas, Edición y Revisión de Inglés Técnico, por la Universitat Rovira i Virgili, o El Cerébro Bilingüe y Aprender a Aprender, por la plataforma COURSERA.',
];
