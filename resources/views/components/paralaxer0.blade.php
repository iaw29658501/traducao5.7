 <div class="container-fluid m-0 p-0 img-text-section">
    <div class="row no-gutters">
        <div class="col-sm-1-12 col-md-6 d-flex align-items-center"
            style="background-color: {{ $color ?? "#1631de" }}">
            <div class="container p-3 mt-lg-4 ml-lg-5 d-flex align-items-start flex-column justify-content-center">
                <h1 class="text-light"><b>{{ $title }} </b></h1>
                <h3 class="text-light medium {{ isset($maxwidth) ? 'max-width-80' : '' }}">{{ $text }} </h3>
            </div>
        </div>
        <div class="col-sm-1-12 col-md-6 m-0 p-0">
            <img src="{{ $img }}" alt="" srcset="" class="img-fluid d-lg-none ">
            <div class="d-none d-lg-block" id="parallaxtest" style="background-image: url('{{ $img }}'); background-position: {{ $imgPosition }}; " ></div>
        </div>
    </div>
</div>
