<?php

return [
    'title' => 'LA TRADUCCIÓN HOY Y LAS NUEVAS TECNOLOGÍAS',
    'alt' => '',
    'subtitle' => 'Siempre actualizados',
    'body' => 'Desde su fundación en 2008, HUMANA colabora en el proyecto de localización de programas de la empresa ucraniana AIT, que produce software específico para agencias de traducción o traductores autónomos. Se trata de programas utilizados como herramientas auxiliares, como son PROJETEX 3D, AnyCount, TO3000 o AceProof, desarrollados para optimizar las tareas de gestión de proyectos de traducción y su control de calidad.
    <br><br>
    Este tipo de servicios es particularmente emblemático, ya sea por el volumen de palabras o por el tipo de texto a ser tratado, y para HUMANA suponen un esfuerzo de actualización en la gestión de los proyectos encargados. Todo ello implica la puesta en funcionamiento de una serie de recursos informáticos y de gestión de procesos de trabajo más actuales en lo que atañe al control de calidad del producto final, el uso de programas específicos de traducción, el trabajo en redes y la gestión del proyecto a lo largo de su desarrollo, que puede hacerse en nube. Todas estas formas de trabajo del universo de la traducción actual conllevan el uso de herramientas auxiliares en aquello que se ha convenido llamar, de forma general, Traducción Asistida por Ordenador (TAO).
    <br><br>
    Así pues, la localización de programas es un ejemplo del cambio fundamental en la ejecución de tareas de gestión de proyectos y de equipos de traducción, ya sea en los servicios de pequeño porte como los de gran envergadura, donde las nuevas tecnologías de la traducción juegan un papel esencial y que altera radicalmente la forma de trabajo de la época de los diccionarios impresos y los ficheros escritos a mano. Las nuevas tecnologías son hoy en día posibles gracias a los logros en el campo de la ingeniería informática que permiten tratar grandes cantidades de texto creando al mismo tiempo bases de datos (memorias de traducción, glosarios y corpus lingüísticos) así como verificar la traducción final.
    <br><br>
    El uso de estas herramientas tecnológicas representa una inversión importante, ya sea en el tiempo dedicado a la calificación y actualización constantes o como costo económico que conlleva, puesto que la industria de la TAO es cada vez más compleja y, al mismo tiempo, relevante para el profesional. A pesar de ello, el empleo de estos programas permite una reducción del tiempo de servicio al dotarlo de un ritmo más acelerado y permitir ganancias en productividad para grandes volúmenes de texto. Todo ello, a su vez, hace posible una mejor competitividad en el mercado y una reducción considerable de costes transferible al cliente.
    <br><br>
    En cuanto al desarrollo de un servicio de traducción, con vista a obtener la calidad de trabajo deseada, utilizamos como referencia las normativas específicas definidas para las distintas etapas por las que todo encargo pasa. Los procesos de control de calidad aplicados por HUMANA descritos a continuación nos dan una idea de las tareas de gestión implícitas en cada servicio donde el control de calidad se da de forma transversal y empieza en el mismo momento en que se recibe el encargo y abarca todo el proceso hasta su entrega.
    '
];
