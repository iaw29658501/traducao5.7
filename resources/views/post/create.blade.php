<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')
<script>
    function updateSlug() {
        let text = document.getElementById('title').value;
        text = text.toLowerCase().replace(/ +/g,'-').normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^\w-]+/g,'').replace(/-+/g, '-').replace(/-$/g,'');
        document.getElementById('slug').value = text;
    }
</script>
<body style="margin-top:10em">
    @include('components.navWhite')

    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js">
</script>
<div class="container">
    @if($errors->any())
    @foreach ( $errors->all() as $error)
    <div class="alert alert-danger">{{ $error }}</div>
    @endforeach
    @endif
    <form action="/pt/post" method="POST">
        @csrf
        <div class="form-group">
            <label for="title" class="col-sm-1-12 col-form-label">Post title:</label>
            <div class="col-sm-1-12">
                <input type="text" class="form-
                    control" name="title" id="title" placeholder="Title" value="{{ old('title') }}"
                    oninput="updateSlug()">
            </div>
        </div>
        <div class="form-group">
            <label for="slug" class="col-sm-1-12 col-form-label">traducaosimultaneabelem.com.br/blog/what?</label>
            <div class="col-sm-1-12">
                <input type="text" class="form-control" name="slug" id="slug" placeholder="example: linguas-indigenas "
                    value="{{ old('slug') }}">
            </div>
        </div>
        <div class="form-group">
            <col-sm-1-12>
                <textarea name="content" value="{{ old('content') }}"></textarea>
            </col-sm-1-12>
        </div>
        <div class="form-group">
            <div class="col-sm-1-10">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
            </div>
        </div>
    </form>
</div>
<script>
    CKEDITOR.replace( 'content' );
        CKEDITOR.instances['content'].setData(`{!! old('content') !!}`);
</script>
@include('components.bootstrap')
@include('components.footer')
</body>

</html>
