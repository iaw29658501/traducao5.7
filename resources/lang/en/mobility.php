<?php

return [
    'title' => 'MOBILITY',
    'subtitle' => 'The need for adaptability',
    'body' => 'The structure and organization of HUMANA COM & TRAD allows the company to have great agility as well as geographical mobility. In Brazil, it has met a demand for interpretation services in different locations away from its headquarters in Belém do Pará, such as Macapá, Porto Velho, Porto Alegre or Maceió. Thus covering the whole country.'
];
