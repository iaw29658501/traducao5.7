@include('components.mouseover')
<style>
    .nav-link>*,
    #lang-selector>* {
        color: white;
    }


</style>
<nav class="navbar navbar-expand-xl navbar-dark bg-dark fixed-top {{ $fixed ?? "" }} ">
    <a class="navbar-brand" href="/{{ app()->getLocale() }}">
        <img src="/imgs/logo.png" alt="">
    </a>

    <button class="navbar-toggler d-xl-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
        aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse navbar" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0 nav-flex-icons">
            <li class="nav-item ">
                <a class="nav-link text-shadow" href="/{{ app()->getLocale() }}/"><b>{{ __('nav.home') }}</b> <span
                        class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><b>{{ __('nav.company') }}</b></a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.presentation')">{{ __('nav.presentation')}}
                    </a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.bases')">{{ __('nav.companyCore') }}</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><b>{{ __('nav.interpretation') }}</b></a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.versatility')">{{ __('nav.versatility') }}</a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.comunicative')">{{ __('nav.comunicativeSituation') }}</a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.mobility')">{{ __('nav.mobility') }}</a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.big-events')">{{ __('nav.bigevents') }}</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><b>{{ __('nav.translation') }}</b></a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.portfolio')">{{ __('nav.portfolio') }}</a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.technology')">{{ __('nav.tranlationAndNewTech') }}</a>
                    <a class="dropdown-item"
                        href="/{{ app()->getLocale() . '/'}}@lang('routes.quality-control')">{{ __('nav.qualityAndProcedures') }}</a>

                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><b>{{ __('nav.10years') }}</b></a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item" href="/{{ app()->getLocale() . '/'}}@lang('routes.presentation')">{{ __('nav.humanInstitute') }}</a>
                    <a class="dropdown-item" href="/{{ app()->getLocale() . "/idc" }}">{{ __('nav.idc') }}</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/{{ app()->getLocale() . '/'}}@lang('routes.contact')"><b>{{ __('nav.contactus') }}</b> <span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/{{ app()->getLocale() }}/blog"><b>Blog</b> <span
                        class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item dropdown align-items-end" id="lang-selector">
                <a class="nav-link dropdown-toggle" href="https://traducaosimultaneabelem.com.br/" id="dropdown09"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                        class="flag-icon flag-icon-{{ __('nav.country') }} "> </span> {{ __('nav.language') }}</a>
                <div class="dropdown-menu" aria-labelledby="dropdown09">
                    <a class="dropdown-item" href="/pt/{{ Lang::get('routes.' . Route::currentRouteName(),[],'pt' ) }}"><span
                            class="flag-icon flag-icon-br"> </span> Português</a>
                    <a class="dropdown-item" href="/en/{{ Lang::get('routes.' . Route::currentRouteName(),[],'en' ) }}"><span
                            class="flag-icon flag-icon-us"> </span> English</a>
                    <a class="dropdown-item" href="/es/{{ Lang::get('routes.' . Route::currentRouteName(),[],'es' ) }}"><span
                            class="flag-icon flag-icon-es"> </span> Español</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav flex-row justify-content-md-center justify-content-start flex-nowrap">
            <li class="nav-item"> <a href="https://www.facebook.com/HUMANA-COM-TRAD-337426299605181/"
                    class="fb-ic nav-link waves-effect waves-light">
                    <i class="fab fa-facebook-f fa-md white-text mr-md-5 mr-3"> </i>
                </a> </li>
            <li class="nav-item"> <a href="https://www.linkedin.com/company/humanacom&trad/"
                    class="li-ic nav-link waves-effect waves-light">
                    <i class="fab fa-linkedin-in fa-md white-text mr-md-5 mr-3"> </i>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/humanacomtrad/" class="ins-ic nav-link waves-effect waves-light">
                    <i class="fab fa-instagram fa-md white-text mr-md-5 mr-3"> </i>
                </a>
            </li>
        </ul>
    </div>
</nav>

