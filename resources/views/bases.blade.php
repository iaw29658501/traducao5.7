@component('components.template')
@slot('image')
    /imgs/as-bases-da-empresa.jpg
@endslot
@slot('alt')
{{ __('bases.alt')  }}
@endslot
@slot('title')
{{ __('bases.title')  }}
@endslot
@slot('subtitle')
{{ __('bases.subtitle')  }}
@endslot
@slot('body')
{!! __('bases.body')  !!}
@endslot

@slot('title2')
{{ __('bases.title2')  }}
@endslot
@slot('subtitle2')
{{ __('bases.subtitle2')  }}
@endslot
@slot('body2')
{!! __('bases.body2')  !!}
@endslot

@slot('title3')
{{ __('bases.title3')  }}
@endslot
@slot('subtitle3')
{{ __('bases.subtitle3')  }}
@endslot
@slot('body3')
{!! __('bases.body3')  !!}
@endslot
@endcomponent
