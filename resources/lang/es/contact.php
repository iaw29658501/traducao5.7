<?php

return [
    'contactus' => 'Contáctenos',
    'yourname' => 'Su nombre',
    'youremail' => 'Su email',
    'subject' => 'Asunto',
    'message' => 'Mensaje',
    'send' => 'Enviar',
    'writetous' => '¡ Escribenos !',
    'address' => '"Espaço Empreendedor, PCT Guamá",
    Av. Perimetral da Ciência
    Km 01, módulo 308
    CEP 66055-110
    Belém – PA Brazil',
    'send' => 'Enviar'
];

