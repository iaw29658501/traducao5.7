
<?php
return [
    'title' => 'R+D',
    'subtitle' => 'Research & Development',
    'body' => 'On the Celebration of the 10th Anniversary of HUMANA and the opening of its new headquarters in the Technological Park of the Federal University of Pará, PCT Guamá, HUMANA launches its Research and Development Department for new technology-based products, partnering with local companies with a focus on the translation and interpretation markets.'
];
