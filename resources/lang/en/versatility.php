<?php

return [
    'title' => 'VERSATILITY',
    'subtitle' => 'Adaptation',
    'alt' => 'Translations for everyone',
    'body' => 'Throughout the first ten years of its trajectory, HUMANA COM & TRAD has provided 233 interpretation services in events on a wide variety of disciplines and themes, notably at congresses, meetings and colloquia on political, legal or economic issues (87), environment (48) and medicine (20). Versatility in terms of the topics covered, as well as the ability to adapt when working in remote places represent an additional values prized by the company. HUMANA COM & TRAD is an enterprise that can be counted on to deploy complex infrastructures and has the human and material resources to meet the most varied communicative situations in locations posing significant logistics challenges.'
];
