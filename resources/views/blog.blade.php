<!DOCTYPE html>
@include('components.htmllang')
@include('components.head')

@include('components.nav')
<body>
   @include('components.carousel')
   <hr>
   <div class="container">
        @include('components.definitions')
   </div>
   @include('components.bootstrap')
   @include('components.footer')
   <link rel="stylesheet" href="/css/style.css">

</body>

</html>
