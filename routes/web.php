<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */



if (in_array(Request::segment(1), Config::get('app.alt_langs'), 1)) {
    App::setLocale(Request::segment(1));
    Config::set('app.locale_prefix', Request::segment(1));
}

/*
 * Set up route patterns - patterns will have to be the same as in translated route for current language
 */

foreach (Lang::get('routes') as $k => $v) {
    Route::pattern($k, $v);
}

Route::group(array('prefix' => Config::get('app.locale_prefix')), function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

    Route::get('/{welcome}', function () {
        return redirect('/'.Config::get('app.locale_prefix'));
    });

    Route::get('/{contact}/', function () {
        return view('contact');
    })->name('contact');
    Route::get('/contato.html', function () {
        return view('contact');
    })->name('contact');
    Route::get('/blog', 'BlogController@index')->name('blog');
    Route::get('/blog.html', 'BlogController@index');
    Route::get('/blog/{post}', 'BlogController@show')->name('blog');
    Route::resource('/post', 'PostController')->middleware('auth');
    Route::get('/{presentation}/', function () {
        return view('presentation');
    })->name('presentation');

    Route::get('/{bases}/', function () {
        return view('bases');
    })->name('bases');
    Route::get('/{versatility}/', function () {
        return view('versatility');
    })->name('versatility');
    Route::get('/{comunicative}/', function () {
        return view('comunicative');
    })->name('comunicative');
    Route::get('/{mobility}/', function () {
        return view('mobility');
    })->name('mobility');

    Route::get('/{bigevents}/', function () {
        return view('bigevents');
    })->name('bigevents');

    Route::get('/{portfolio}/', function () {
        return view('portfolio');
    })->name('portfolio');
    Route::get('/{technology}/', function () {
        return view('newtech');
    })->name('newtech');
    Route::get('/{quality}/', function () {
        return view('qualityControl');
    })->name('qualityControl');

    Route::get('/{institut}/', function () {
        return view('institution');
    })->name('institut');

    Route::get('/{idc}/', function () {
        return view('idc');
    })->name('idc');

});
// Route::get('/pt/a-empresa/apresentacao.html','')
Auth::routes(['register' => false]);

Route::get('/home', function() {
    return redirect('/');
})->name('home');

Route::get('/contactme', 'ContactController@index')->middleware('auth');
Route::get('/contactme-del/{contactme}', 'ContactController@delete')->middleware('auth');

Route::post('/contactme', 'ContactController@store');


/* // ADMIN
Route::middleware('auth')->group(function() {
    Route::get('ckeditor', 'CkeditorController@index');
    Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

})
 */
