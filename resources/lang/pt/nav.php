<?php

return [
    'home' => 'Inicio',
    'company' => 'A Empresa',
    'presentation' => 'Apresentação',
    'companyCore' => 'As bases de nossa empresa',
    'interpretation' => 'Interpretação',
    'versatility' => 'Versatilidade',
    'mobility' =>'Mobilidade',
    'bigevents' =>'Eventos Emblemáticos',
    'comunicativeSituation' => 'Situação comunicativa',
    'translation' => 'Traduções',
    'portfolio' => 'Portfólio de clientes e serviços',
    'tranlationAndNewTech' => 'Tradução hoje e as novas técnologias',
    'qualityAndProcedures' => 'Processos e controle de qualidade',
    '10years' => '10 anos',
    'humanInstitute' => 'Instituto Humana',
    'idc' => 'ID+C (PCT GUAMÁ)',
    'country' => 'br',
    'language' => 'Português',
    'contactus' => 'Contato'
];

