<div class="container">
<div class="d-flex flex-column flex-md-row justify-content-around align-items-top mb-4 mt-4">
    <div class="text-center">
        <img src="/imgs/logos/translation.png" alt="traduções">
        <h5 class="font-weight-bold">@lang('home.tradandinterp')</h5>
    </div>
    <div class="text-center">
        <img src="/imgs/logos/instituto-humana.png" alt="instituto humana">
        <h5 class="font-weight-bold">@lang('home.humaninstitute')</h5>
    </div>
    <div class="text-center">
        <img src="/imgs/logos/pesquisa-e-desenvolvimento.png" alt="pesquisa e desenvolvimento">
        <h5 class="font-weight-bold">@lang('home.researchanddevelopment')</h5>
    </div>
</div>
</div>
