<?php

return [
    'title' => 'PRESENTATION',
    'subtitle' => 'The development of the company',
    'body' => 'HUMANA COM & TRAD is a language services company based in Belém do Pará, in the Brazilian Amazon since 2008. Its founder and director is Sandro Ruggeri Dulcet, professional translator and interpreter since 1995, the year it began providing services to several local clients and businesses in the industry.
    For the last decade,  HUMANA COM & TRAD has provided more than 300 interpretation and translation services of which 233 correspond to interpretation services (75%) and 92 to translation jobs. The company now has a portfolio of 263 customers among fixed and occasional clients. Thirty-nine of our professional translators have provided translation and proofreading services. Our interpretation team gathers 97 interpreters, both local and from several cities in Brazil, French Guiana, Suriname and Guyana.
    HUMANA’S geographical distribution has extended its activities to all of Brazil; reaching as far as Porto Alegre, Brasília, Maceió, Porto Velho and Macapá, among others. It has also worked in cities of the Guiana Shield, such as Cayenne, Paramaribo and Georgetown, and also Tunis and Dakar, in Africa.
    As for written translation services, the company has managed more than 120 translation projects, an approximate 1,800,000 words in this period, mainly text in Portuguese, English, Spanish, French and Italian. The most recurrent themes, both in translation and interpreting services, are environmental issues, scientific research (zoology, anthropology, botany), social policies, sustainable forest management, sustainable social and economic development, information technology etc. HUMANA has collaborated in software localization projects for companies in the translation industry. In addition, it regularly provides sworn translation services in all languages ​​and combinations of Portuguese and English.'
];

