<?php

return [
    'title' => 'EVENTOS EMBLEMÁTICOS',
    'subtitle' => 'ESTUDIO DE CASO',
    'body' => 'Con relación a los eventos atendidos fuera de Brasil, cabe destacar las actividades en el Escudo de las Guayanas, donde HUMANA ha participado en congresos científicos y medioambientales, como el Congreso sobre la Biodiversidad Social y Ecológica, en sus ediciones II en Macapá, III en Paramaribo y IV en Georgetown. Ha atendido también al Office National des Fôrets durante los dos años de duración del proyecto REDD+ para el Escudo de las Guayanas, una serie de 14 reuniones técnicas pautadas por la agenda medioambiental de la ONU, donde grupos de trabajo formados por técnicos forestales y tomadores de decisión de los distintos países mantuvieron encuentros donde se crearon protocolos comunes de análisis de datos, evaluación de desafíos y propuestas de políticas ambientales.
    <br> <br>
    Estas actividades, con encuentros periódicos itinerantes en las principales ciudades de la región y también en lugares alejados de las capitales, se destacan particularmente por los desafíos técnicos y logísticos que comportan, en especial por los desplazamientos de equipamiento y personal a lo largo de todo el territorio de las Guayanas.
    <br> <br>
    Otro grupo de eventos, igualmente importantes y complejos, está formado por las reuniones y seminarios de la edición del Fórum Social Mundial (FSM) celebrado en Belém en 2009, así como el Congreso de Educadores de Arte, IDEA 2010.
    <br> <br>
    En el caso del FSM se atendieron 12 actividades en situaciones comunicativas diversas y para públicos distintos, donde se trataron, entre otros, temas de tipo religioso (Fórum Mundial de Teología y Liberación), jurídico (Fórum Mundial de Jueces), educativo (Fórum Mundial de la Educación), político o reivindicativo (Fórum Mundial de las Autoridades Locales, Fórum Mundial de la Vía Campesina). Asimismo, para atender reuniones puntuales del grupo de trabajo organizador del Fórum Social Mundial, el equipo de HUMANA COM & TRAD se desplazó en tres ocasiones a África: Dakar, Senegal, en 2015, y Túnez, en 2011 y 2013.
    <br><br>
    El encuentro de educadores de arte IDEA 2010 representó un caso especialmente complejo de organización en un evento multitudinario, donde HUMANA COM & TRAD consiguió ofrecer soluciones logísticas viables y eficientes ante situaciones comunicativas que representaron desafíos muy variados. Se desplegaron intérpretes profesionales (25) y voluntarios (70) en las cuatro lenguas oficiales (portugués, castellano, inglés y francés), así como técnicos (3), asistentes (13) y receptores de interpretación simultánea (2.000). Las reuniones se dieron tanto en un auditorio central, con todas las instalaciones técnicas necesarias, como en pequeñas salas de aula en encuentros paralelos y que llegaron a suceder en 18 lugares distintos concomitantemente.
    '
];

