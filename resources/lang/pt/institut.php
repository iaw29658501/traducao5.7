<?php

return [
    'title' => 'INSTITUTO HUMANA',
    'subtitle' => 'Novas Perspectivas > Cursos',
    'body' => 'O Instituto HUMANA recolhe, dá continuidade e amplia as atividades levadas a cabo até o momento pela empresa HUMANA COM & TRAD no que tange à difusão de conhecimentos e formação de recursos humanos para a área de interpretação simultânea e tradução escrita, assim como outros temas de interesse dentro da cultura humanística em geral.

    A companhia conta com um histórico de cursos de formação de novos intérpretes desde 2009 até hoje. A maioria dos seus formandos, após um período de estágio, começaram a trabalhar na empresa ou por conta própria. Por conta disso, o Instituto HUMANA conta desde a sua fundação com recursos didáticos para o ensino de línguas e uma extensa bibliografia específica de técnicas de tradução e interpretação.

    Recolhem-se e promovem-se igualmente no Instituto as atividades de formação continuada atendidas pelo seu diretor, dentre as quais destacam a participação nos seguintes congressos: Abrates (2007), como palestrante sobre a situação da tradução e da interpretação na região Norte do Brasil, o Simb! (2013) e Elia Together (2016), como participante. Ou os cursos a distância atendidos, dentre os quais cabe destacar a especialização em Tradução Inglês-Espanhol (Uned) ou o de Tradução Espanhol-Português (Gama Filho-Estácio de Sá), e minicursos sobre temas específicos pela Universitat Rovira i Virgili Edição e Revisão de Inglês Técnico, ou pela plataforma de auto aprendizado Coursera, O Cérebro Bilíngue e Aprendendo a Aprender.'
];
