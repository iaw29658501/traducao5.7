<?php

return [
    'title' => 'MISIÓN',
    'subtitle' => 'Nuestra razón de ser',
    'body' => 'La misión de HUMANA COM & TRAD es prestar servicios de traducción e interpretación de alto nivel, con calidad y competencia, atrayendo, atendiendo y fidelizando a sus clientes. Para alcanzarlo la empresa sigue procesos estandarizados y, al mismo tiempo, personalizados de atención donde establece un compromiso duradero con su red de colaboradores, suministradores, clientes y usuarios.
    <br><br>
    La empresa atiende un público variado de investigadores, profesores y estudiantes universitarios, funcionarios y miembros de instituciones de investigación y difusión de conocimiento, y ONGs.  Sus encargos principales corresponden a traducciones y revisiones de artículos científicos, publicaciones y ensayos de temas variados, manuales técnicos o textos informativos, así como servicios de interpretación simultánea en conferencias internacionales, en su modalidad de cabina o interpretación consecutiva y de acompañamiento.
    <br><br>
    La prestación de estos servicios se entiende tanto desde su perspectiva económica, como actividad puntual contratada con fecha y hora de entrega y un valor previamente acordado, como también se llevan en cuenta las relaciones que se establecen con el cliente, los suministradores y colaboradores, con quienes se establecen vínculos profesionales y humanos de confianza y mutuo respeto, desarrollo y crecimiento de larga duración. La actividad empresarial de HUMANA crea así un valor añadido donde la prestación del servicio se entiende como una acción de confluencia, de colaboración mutua, en la que todos los participantes al desarrollar diferentes tareas contribuyen de forma igual con su conocimiento y entusiasmo.',
    'title2'=>'VISIÓN',
    'subtitle2'=>'Dónde queremos estar en los próximos años',
    'body2'=>'Pretendemos continuar siendo una empresa líder y distinguida en la prestación de servicios de calidad, creadora de valor para quien en ella participa, comprometida su entorno social y ambiental, responsable y justa, con actividades sostenibles a lo largo del tiempo donde colaboradores, suministradores, clientes y usuarios se consideren parte integrante de una dinámica de comunicación e intercambio de experiencias enriquecedoras. Queremos seguir contribuyendo para la creación de un ambiente favorable de crecimiento profesional y personal de todos los que trabajan en el campo de la traducción en Brasil.',
    'title3'=>'VALORES',
    'subtitle3'=>'En qué creemos y cómo somos',
    'body3'=>'Los valores que nos inspiran y guían la estrategia y forma de actuar de la empresa son la dedicación y el entusiasmo por el trabajo realizado, la organización, la clareza y cuidado así como el uso racional en el trato de los recursos humanos y materiales disponibles. Forman parte de los valores de HUMANA el incentivo constante de sus colaboradores para que desarrollen su potencial y crezcan personal y profesionalmente. En contrapartida, el alto grado de exigencia con relación a la actitud profesional, organización, puntualidad y entrega conforme a  plazos son fundamentales en el momento de valorar la contribución y confiabilidad, valores estos tan o más importantes que el servicio en sí.',
];

