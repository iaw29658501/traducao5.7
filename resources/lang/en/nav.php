<?php

return [
    'home' => 'Home',
    'company' => 'About our Company',
    'presentation' => 'Presentation',
    'companyCore' => 'Our Company Core',
    'interpretation' => 'Interpretation',
    'versatility' => 'Versatility',
    'mobility' =>'Mobility',
    'bigevents' =>'Emblematic Events',
    'comunicativeSituation' => 'Comunicative Situation',
    'translation' => 'Our translations',
    'portfolio' => 'Portfolio of customers and services',
    'tranlationAndNewTech' => 'Translation today and new technologies',
    'qualityAndProcedures' => 'Procedures and quality control',
    '10years' => '10 years',
    'humanInstitute' => 'Instituto Humana (Human Institute)',
    'idc' => 'Research & Development',
    'country' => 'us',
    'language' => 'English',
    'contactus' => 'Contact Us'
];

