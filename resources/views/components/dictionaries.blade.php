<div class="container">
    <div class="jumbotron py-sm-1 py-lg-2 d-flex align-self-center mt-5">
        <div class="row justify-content-between">
            <div class="col col-md-9 pb-0">
                <h1><b>{{ $title }}</b> </h1>
                <h4>{{ $grammar }}</h4>
                <p class="lead">{{ $def }}</p>
                <br><br>
            </div>
            <div class="col col-md-2 p-0 d-flex d-md-block justify-content-center">
                <br class="d-none d-md-"><br ><br class="d-md-none"><br class="d-md-none">
                <img src="{{ $img }}" class="align-self-center" alt="interpretations" srcset="">
            </div>
        </div>
    </div>
</div>