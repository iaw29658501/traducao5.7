<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')

@include('components.nav')
<script src="https://google.com/recaptcha/api.js"></script>
<body>
<div class="container-fluid pt-5">

   @include('components.contactussection')
   @include('components.bootstrap')
   @include('components.footer')
</div>
<link rel="stylesheet" href="/css/style.css">

</body>

</html>
