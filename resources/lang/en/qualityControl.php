<?php

return [
    'title' => 'PROCESSES AND QUALITY CONTROL',
    'subtitle' => 'Full attention to details',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'To guarantee the quality throughout its management and until the final translation, once the service is commissioned, a system of different levels is established, following the following steps:
        <br><br>
        • Selection of the translation team.
<br><br>
        • Workflow where a cross-checking system with peer review is guaranteed.
<br><br>
        • Use of the most cutting-edge technology for content management, documentation management and reporting.
<br><br>
        • A project management system that ensures that all customer demands are met, allowing a quick response to last-minute requests or unmarked changes.
    More specifically, the Principles and Methodology of Translation Quality Management that the company adopts are as follows:
        • With regard to the choice of the translator, the following questions are answered: What is his or her experience in translation or in the specific area of the order? Has he or she translated over the last few years?
    In this way, the configuration of the translation team is made according to the specific needs of the project and represents the first step towards an efficient and solid Translation Quality Management.
<br><br>
        • In relation to the technologies, the choice of the most appropriate CAT (Computer-Assisted Translation) tool will be made according to project specifications, number of participants and work system. In general, the company uses the most advanced technologies in Translation Memories and Terminology Management, such as the memoQ software or the Smartcat network work platform. In both cases the programs allow the same sentences to be translated identically and the terminology is consistently used throughout the project, even if different translators work in different parts of the same text.
<br><br>
        • On the editing process, aiming to minimize the possibility of errors and omissions in information or language, the same text goes through a translator and a reviewer, native professionals of the target language, who possess the specific qualifications necessary to the project.
<br><br>
        • With respect to the Translation Project Management, the following guidelines are followed:
        <br><br>
            ◦ the adequacy of the flow of the translation process to the specific needs of the client.
<br><br>
            ◦ The preparation and monitoring of quality control lists.
<br><br>
            ◦ proactive communication with the client for the solution of linguistic or technical issues.
<br><br>
            ◦ the maintenance of glossaries and multilingual translation memories specific to each project.
<br><br>
            ◦ The collection of individual documentation of the project history.     '
    ];


