<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')

<body>
    @include('components.nav')
    <br><br>

<div class="container text-center">
    <br><br>
            <h1>{{ $post->title }}</h1>
            <div class="container text-justify">
                <p class="lead">{!! $post->content !!} </p>
            </div>
        @if (Auth::check())
        <form action="/{{ app()->getLocale() }}/post/{{ $post->slug  }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger m-5">DELETE</button>
        </form>
        @endif
</div>
@include('components.bootstrap')
@include('components.footer')
<link rel="stylesheet" href="/css/style.css">
</body>

</html>
