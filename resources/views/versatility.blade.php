@component('components.template')
@slot('image')
/imgs/versatilidade-traducao-em-belem.jpg
@endslot
@slot('logo')
/imgs/logos/versatilidade.png
@endslot
@slot('alt')
{{ __('versatility.alt')  }}
@endslot
@slot('title')
{{ __('versatility.title')  }}
@endslot
@slot('subtitle')
{{ __('versatility.subtitle')  }}
@endslot
@slot('body')
{{ __('versatility.body')  }}
@endslot
@endcomponent
