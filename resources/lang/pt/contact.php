<?php

return [
    'contactus' => 'Contate-nos',
    'yourname' => 'Seu nome',
    'youremail' => 'Seu email',
    'subject' => 'Assunto',
    'message' => 'Mensagem',
    'send' => 'Enviar'
];

