@component('components.template')
@slot('nav')
    white
@endslot
@slot('navdontchange')
dont
@endslot
@slot('image')
    /imgs/nice/eventos-emblematicos.jpg
@endslot
@slot('logo')
    /imgs/qualidade.png
@endslot
@slot('alt')
{{ __('qualityControl.alt')  }}
@endslot
@slot('title')
{{ __('qualityControl.title')  }}
@endslot
@slot('subtitle')
{{ __('qualityControl.subtitle')  }}

@endslot
@slot('body')
{!! __('qualityControl.body') !!}
@endslot
@endcomponent
