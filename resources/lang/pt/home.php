<?php

return [
    'aboutus' => 'Sobre nós',
    'translation' => 'Tradução',
    'translationGrammar' => '[substantivo, s.f.] ',
    'translationDef' => 'Transformar exatamente [sem ambiguidades], uma palavra/frase/texto de uma lingua para a outra',
    'interpretation' => 'Interpretação',
    'interpretationGrammar' => '[substantivo, s. f.]',
    'interpretationDef' => 'Ação que consiste em estabelecer, simultânea ou consecutivamente, comunicação verbal ou não verbal entre duas entidades, pessoas ou entre um(a) falante e seu público.',
    'mission' => 'Missão',
    'missionText' => 'A missão da HUMANA COM & TRAD é oferecer serviços de tradução e interpretação de alto nível, com qualidade e competência, atraindo, atendendo e fidelizando os seus clientes.',
    'vision' => 'Visão',
    'visionText' => 'A empresa HUMANA COM & TRAD comemora, no próximo mês de abril de 2019, 10 anos da sua fundação. Essa efeméride traz de volta lembranças de uma década de experiências profissionais e aprendizados. Desejamos compartilhar com todos os que tornaram esse sonho realidade.',
    'values'=>'Valores',
    'valuesText'=>'Os valores que nos inspiram e guiam a estratégia e forma de agir da empresa são a dedicação e o entusiasmo no trabalho, a organização, a clareza, o cuidado e o uso racional no tratamento dos recursos humanos e materiais.',
    'banner' => 'Tradutores desde as linguas mais famosas até
    as minoritaras do Brasil',
    'translators'=>'Tradutores',
    'interpreters' => 'Intérpretes',
    'translatedWords' => 'Palavras traduzidas',
    'tradandinterp' => 'Tradução & Interpretação',
    'humaninstitute' => 'Instituto Humana',
    'researchanddevelopment' => 'Pesquisa e desenvolvimento'
];

