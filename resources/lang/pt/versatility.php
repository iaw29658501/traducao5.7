<?php

return [
    'title' => 'VERSATILIDADE',
    'subtitle' => 'Adaptação',
    'alt' => 'Tradução Brasil',
    'body' => 'Ao longo dos primeiros dez anos da sua trajetória, a HUMANA COM & TRAD prestou 233 serviços de interpretação em eventos de diversas disciplinas e temáticas. Destacam-se os congressos, reuniões e colóquios sobre temas do âmbito político, jurídico ou econômico (87), meio ambiente (48) e medicina (20). A versatilidade no que se refere aos temas tratados, assim como a capacidade de adaptação na hora de trabalhar em lugares remotos, representam um valor adicional que a empresa potência ao máximo. A HUMANA COM & TRAD é uma companha com a qual se pode contar na hora de montar infraestruturas complexas: dispõe de recursos humanos e materiais para atender as mais variadas situações comunicativas em lugares com grandes desafios de logística.'
];
