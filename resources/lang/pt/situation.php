
<?php
return [
    'title' => 'I+D',
    'subtitle' => 'Pesquisa e desenvolvimento',
    'body' => 'Coincidindo com a comemoração do seu 10 Aniversário a HUMANA inaugura a sua nova sede no Parque Tecnológico da Universidade Federal do Pará, PCT Guamá. Neste espaço serão desenvolvidos novos produtos de base tecnológica em parceria com empresas locais, voltados ao mercado da tradução e interpretação.'
];
