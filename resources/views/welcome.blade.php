<!DOCTYPE html>
@include('components.htmllang')

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ __('head.title') }}</title>
    <meta name="keywords"
        content="tradução em belem, tradução simultanea em belem, serviço de tradução em belem, serviço de tradução simultanea em belem, tradutor em belem, tradutores em belem, tradutor profissional em belem, tradução de documentos em belem" />
    <meta name="description"
        content="Precisando de serviço de tradução em Belém? A HUMANA COM &amp; TRAD é uma empresa prestadora de serviços linguísticos presente na Amazônia e apta a oferecer seus serviços em todo o Brasil. Consulte-nos!" />
    <link href="/imgs/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <!-- Required meta tags -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- social icons -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <!-- language flags -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
</head>
@include('components.nav')
@include('components.jquery')
<script src="/js/navColorAndDropDown.js"></script>

<body>
    @include('components.banner')
    <hr>
    {{-- carrousel --}}
    @include('components.dictionary')
    <hr>
    @component('components.paralaxer0')
    @slot('title')
    {{ __('home.mission') }}
    @endslot
    @slot('maxwidth')
    yes
    @endslot
    @slot('text')
    {{ __('home.missionText') }}
    @endslot
    @slot('color')
    #1e50f7
    @endslot
    @slot('img')
    /imgs/parallax-kid.png
    @endslot
    @slot('imgPosition')
    right
    @endslot
    @endcomponent
    <hr>
    @include('components.institutionLogos')
    <hr>
    @component('components.paralaxer1')

    @endcomponent
    <hr>
    @include('components.numbers')
    <hr>

    @component('components.paralaxer0')
    @slot('title')
    {{ __('home.values') }}
    @endslot
    @slot('text')
    {{ __('home.valuesText') }}
    @endslot
    @slot('color')
    #1e50f7
    @endslot
    @slot('img')
    /imgs/valores-parallax.png
    @endslot
    @slot('imgPosition')
    right
    @endslot
    @endcomponent
    {{--  @include('parallaxscript') --}}
    <!-- Social Media -->
    <script src="https://kit.fontawesome.com/acd9286862.js" crossorigin="anonymous"></script>
    <!-- Optional JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <hr>
    @include('components.footer')
    <link rel="stylesheet" href="/css/style.css">
</body>

</html>
