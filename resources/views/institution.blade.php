@component('components.template')
@slot('image')
/imgs/instituto-humana.jpg
@endslot
@slot('logo')
/imgs/instituto-humana-int.png
@endslot
@slot('alt')
{{ __('institut.alt')  }}
@endslot
@slot('title')
{{ __('institut.title')  }}
@endslot
@slot('subtitle')
{{ __('institut.subtitle')  }}
@endslot
@slot('body')
{!! __('institut.body')  !!}
@endslot
@endcomponent
