<?php

return [
    'contactus' => 'Contact us',
    'writetous' => 'Write to us !',
    'youremail' => 'Your email',
    'yourname' => 'Your name',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send',
    'address' => '"Espaço Empreendedor, PCT Guamá",
    Av. Perimetral da Ciência
    Km 01, módulo 308
    CEP 66055-110
    Belém – PA Brazil',
    'send' => 'Send'
];
