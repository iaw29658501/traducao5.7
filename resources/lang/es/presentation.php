<?php

return [
    'title' => 'PRESENTACIÓN',
    'subtitle' => 'La empresa y su desarrollo.',
    'body' => 'HUMANA COM & TRAD es una empresa de servicios lingüísticos con sede en la ciudad de Belém do Pará,  Amazonia brasileña, cuyas actividades empiezan en 2008. Su fundador y director ejecutivo es Sandro Ruggeri Dulcet, traductor e intérprete profesional autónomo desde 1995, año en que empieza a trabajar atendiendo a diferentes clientes y empresas locales del sector.
    <br><br>
    En una década, ha prestado <b>300 conferencias y reuniones internacionales</b>, de los cuales 233 corresponden a servicios de interpretación (75%) y 92 a servicios de traducción. La empresa cuenta hoy con una cartera de 263 clientes, entre estables y esporádicos. Hasta la fecha, han colaborado 39 traductores y traductoras autónomos, quienes han ofrecido servicios de traducción y revisión, y <b>97 intérpretes</b>, de los cuales 27 son locales y el resto provienen de distintas ciudades de Brasil y del Escudo de las Guayanas (Guayana Francesa, Surinam y Guayana).
    <br><br>
    En lo relativo a la distribución geográfica, ha desarrollado sus actividades en diferentes lugares de Brasil, lejos de su sede, como Porto Alegre, en el sur, Brasilia, Maceió, en el oeste, Porto Velho, en el este, y Macapá, en el extremo norte.  Ha trabajado igualmente en ciudades del Escudo de las Guayanas, como Cayena, Paramaribo o Georgetown, e, incluso, africanas, como Túnez y Dakar.
    <br><br>
    Con relación a los servicios de traducción escrita, la empresa ha gestionado durante este período un volumen de trabajo de <b>1 800 000 palabras, principalmente de y entre los siguientes idiomas: portugués, inglés, castellano, francés e italiano.</b> Los temas con más presencia, ya sea en la traducción escrita como en los servicios de interpretación, son las cuestiones medioambientales, científicas (zoología, antropología, botánica), políticas sociales, gestión sostenible de los recursos forestales, desarrollo social y económico sostenibles, informática, etc. Ha colaborado también en la localización de programas de empresas del sector de la traducción. Atiende, igualmente, servicios de traducción jurada de documentos oficiales en todas las lenguas y sus combinaciones de y hacia el portugués.'
];

