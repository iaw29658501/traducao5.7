<?php

return [
    'title' => 'SITUACIÓN COMUNICATIVA',
    'subtitle' => 'Los detalles de los eventos',
    'alt' => 'Servicio de traducción simultánea en todo Brasil.',
    'body' => 'Con tal de llevar a cabo las tareas encargadas, HUMANA COM & TRAD analiza las condiciones particulares de cada evento para decidir qué medios humanos y técnicos son los más adecuados. Diseñamos material técnico especial para viajes de larga distancia, de fácil montaje y dispone de un equipo humano altamente calificado y dispuesto a trabajar en condiciones extremas.
    <br><br>
    Asimismo, desde su fundación, la empresa ha puesto a disposición una serie de cursos de capacitación para jóvenes traductores e intérpretes, encargados al Instituto HUMANA y llevados a cabo en colaboración con centros universitarios locales o de todo Brasil. En ese sentido, la tarea de formación ha sido fundamental en la transformación del panorama de escasez de profesionales, creando un ambiente propicio a la  ejecución de eventos de gran envergadura (Fórum Social Mundial, IDEA 2010).
    '
];
