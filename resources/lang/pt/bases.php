<?php

return [
    'title' => 'MISSÃO',
    'subtitle' => 'O porquê da empresa',
    'body' => 'A missão da HUMANA COM & TRAD é oferecer serviços de tradução e
                interpretação de alto nível, com qualidade e competência, atraindo, atendendo e fidelizando os seus
                clientes. Para chegar a isso a empresa segue processos padronizados e, ao mesmo tempo, personalizados de
                atenção, com os quais constrói um compromisso duradouro com a sua rede de Colaboradores, Fornecedores,
                Clientes e Usuários.
                <br><br>
                A empresa atende a um público variado de pesquisadores, professores e estudantes universitários,
                funcionários e membros de instituições de pesquisa e divulgação do conhecimento, além de ONGs. Suas
                principais encomendas são traduções e revisões de artigos científicos para publicação, ensaios de
                temáticas variadas, manuais técnicos e textos informativos, assim como serviços de interpretação
                simultânea em conferências internacionais, na sua modalidade de cabine ou de interpretação consecutiva e
                de acompanhamento.
                <br><br>
                A prestação destes serviços não é entendida apenas dentro de uma perspectiva econômica, como atividade
                pontual contratada com data e hora de entrega e valor previamente acordado entre as partes, mas, também,
                como um campo de relações que a empresa estabelece com clientes, fornecedores e colaboradores, onde se
                criam vínculos profissionais e humanos de confiança e respeito mútuo, desenvolvimento e crescimento com
                desdobramentos positivos a longo prazo. A atividade empresarial que a HUMANA enseja cria assim um valor
                adicional, onde a prestação de serviço representa uma ação aglutinadora na qual todos participam
                contribuindo com seu conhecimento e entusiasmo.
',
    'title2'=>'VISÃO',
    'subtitle2'=> 'A nossa empresa para os próximos anos',
    'body2'=>'Desejamos continuar sendo uma empresa líder e diferenciada no mercado de prestadores de serviços linguísticos de qualidade, criando valor adicional para quem dela participa direta ou indiretamente, comprometida com o entorno social e ecológico, responsável e justa, com atividades sustentáveis ao longo do tempo, onde Colaboradores, Fornecedores, Clientes e Usuários sintam-se parte integral de uma dinâmica de comunicação e de troca de experiências enriquecedoras. Desejamos seguir contribuindo na criação de um ambiente favorável de crescimento profissional e pessoal de todos aqueles que trabalham no campo da tradução e a interpretação no Norte do Brasil.',
    'title3'=>'VALORES',
    'subtitle3'=> 'O nosso credo e nossa forma de ser',
    'body3'=>'Os valores que nos inspiram e guiam a estratégia e a forma de agir da empresa são a dedicação e o entusiasmo no trabalho, a organização, a clareza, o cuidado e o uso racional no tratamento dos recursos humanos e materiais disponíveis. Formam parte dos valores da HUMANA o incentivo constante aos seus colaboradores para que desenvolvam todo o seu potencial e cresçam pessoal e profissionalmente. Por outro lado, o alto grau de exigência quanto à atitude profissional, organização, horários e prazos definidos é fundamental na avaliação da contribuição e confiabilidade, valores estes tão ou mais importantes do que o trabalho em si.',

];

