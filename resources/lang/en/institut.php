<?php

return [
    'title' => 'INSTITUTO HUMANA',
    'subtitle' => 'New Prospects > Courses',
    'body' => 'HUMANA INSTITUTE collects, continues and extends the activities carried out so far by HUMANA COM & TRAD. The company has had a history of training courses for new interpreters since 2009. Most of their trainees after a probationary period begin working in the company, or on their own pratctices. This is the reason why the Institute counts, from its foundation, on teaching resources for language learning and an extensive bibliography of translation and interpretation techniques.
    <br><br>
    Also, the HUMANA Institute pursues and fosters the continuing education activities attended by its founder and director, Sandro Ruggeri Dulcet, among which are the participation in the following congresses: ABRATES (2007), as speakers on the situation of translation and interpretation in the Northern region of Brazil, SIMB! (2013) and ELIA TOGETHER (2016) as a participant. And the distance courses attended such as the specialization in English-Spanish Translation (UNED), Spanish-Portuguese Translation (Gama Filho-Estácio de Sá), and mini courses on specific subjects by Universitat Rovira i Virgili Edition and Review of Technical English, and The Bilingual Brain and Learning to Learn by the self-learning platform COURSERA, among others.'
];
