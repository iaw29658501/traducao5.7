<?php

return [
    'title' => 'EVENTOS EMBLEMÁTICOS',
    'subtitle' => 'A nossa história nos avaliza',
    'body' => 'Com relação aos eventos atendidos fora do Brasil, cabe destacar as atividades no Escudo das Guianas, onde a HUMANA participou de encontros científicos sobre meio ambiente, como o Congresso sobre a Biodiversidade Social e Ecológica, nas suas edições 2a em Macapá, 3a em Paramaribo e 4a em Georgetown. Esteve igualmente presente atendendo o Office National des Forêts ao longo da implementação do projeto REDD+ para o Escudo das Guianas, uma série de 14 reuniões técnicas pautadas pelas diretrizes da agenda do meio ambiente da ONU, onde grupos de trabalho formados por técnicos florestais e tomadores de decisões dos diferentes países debateram a criação de protocolos comuns para a análise de dados, avaliação de desafios e propostas de políticas ambientais.
    <br> <br>
    Estas atividades, com encontros itinerantes pelas principais cidades da região e também em lugares distantes das capitais, se destacam pelos desafios técnicos e logísticos que comportam, especialmente os deslocamentos de equipamentos e de pessoal ao longo do território das Guianas.
    <br> <br>
    Um outro grupo de eventos, igualmente relevantes e complexos, é composto pelas reuniões e seminários da edição do Fórum Social Mundial (FSM) celebrada em Belém em 2009, assim como o congresso de arte educadores, Idea 2010. No caso do Fórum Social Mundial 2009 foram atendidas 12 atividades em situações comunicativas bem diversas e com público variado, onde se trataram, entre outros assuntos, temáticas de cunho religioso (Fórum Mundial de Teologia e Libertação), jurídico (Fórum Mundial de Juízes), educativo (Fórum Mundial da Educação), político ou reivindicativo (Fórum Mundial das Autoridades Locais, Fórum Mundial da Via Campesina). Para atender também às reuniões pontuais de grupos de trabalho do Fórum Social Mundial, a equipe da HUMANA COM & TRAD viajou em três ocasiões para a África: para Dakar, Senegal, em 2011 e para Túnis, na Tunísia, em 2013 e 2015.
    <br> <br>
    O encontro de educadores em arte Idea 2010 representa um caso de organização de evento especialmente complexo, onde a HUMANA COM & TRAD ofereceu soluções logísticas viáveis e eficientes, atendendo adequadamente aos desafios apresentados: foram escalados intérpretes profissionais (25) e voluntários (70) que trabalharam nas quatro línguas oficiais do evento (português, espanhol, inglês e francês), assim como técnicos (3), recepcionistas (13) e equipamentos de tradução simultânea (2.200). Os encontros ocorreram tanto num auditório central, com instalações técnicas adequadas, como em pequenas salas de aula improvisadas para reuniões paralelas, que se realizaram em até 18 lugares ao mesmo tempo.'
];

