<?php

return [
'aboutus'=> 'About Us',
'translation' => 'Translation',
'translationGrammar' => '[noun, s.]',
'translationDef' => 'The transformation of word, phrase or text from one language into another without ambiguity, except when ambiguity is a feature of the former.',
'interpretation' => 'Interpretation',
'interpretationGrammar' => '[noun, s.]',
'interpretationDef' => 'Establishing verbal and /or non verbal communication among speaking and listening parties, through simultaneous or consecutive actions.',
'mission' => 'Mission',
'missionText' => 'HUMANA COM & TRAD\'s mission is to offer high-level translation and interpretation services, with quality and competence, attracting, serving and retaining costs for it.',
'vision' => 'Vision',
'visionText' => 'We wish to continue being a leading and differentiated company in the market of quality language-service providers, creating added value for those who directly or indirectly participate in it, committed to the social and ecological environment, responsible and fair, with sustainable activities over time, where Collaborators, Suppliers, Clients and Users feel to be an integral part of a dynamics of communication and exchange of enriching experiences. We wish to continue to contribute to the creation of a favorable environment for professional and personal growth of all those working in the field of translation and interpretation in Brazil and other countries.',
'values'=>'Values',
'valuesText'=>'The values ​​that inspire us and guide the company\'s strategy and action are dedication and enthusiasm at work, organization, transparency, care and rational use in the treatment of available human and material resources. HUMANA values ​​the constant encouragement of its employees to develop their potential and grow professionally and personally. On the other hand, the high degree of professional attitude, organization, schedules and defined deadlines is fundamental in the assessment of contribution and reliability, values ​​that are as important as the work on its own.',
'banner' => 'Translators from the most famous languages to
minoritarian languages from Brazil',
'translators'=>'Translators',
'interpreters' => 'Interpreters',
'translatedWords' => 'Translated Words',
'tradandinterp' => 'Translation and Interpretation',
'humaninstitute' => 'Instituto Humana',
'researchanddevelopment' => 'Research and Development'
];

