@component('components.template')
@slot('image')
    /imgs/portfolio-de-clientes.jpg
@endslot
@slot('logo')
    /imgs/portfolio.png
@endslot
@slot('alt')
{{ __('portfolio.alt')  }}
@endslot
@slot('title')
{{ __('portfolio.title')  }}
@endslot
@slot('subtitle')
{{ __('portfolio.subtitle')  }}

@endslot
@slot('body')
{!! __('portfolio.body') !!}
@endslot
@endcomponent
