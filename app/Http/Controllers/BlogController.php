<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function index() {
        $posts = Post::orderBy('id','desc')->paginate(5);
        return view('post.index', compact('posts'));
    }
    public function show(Post $post) {
        return view('post.show', ['post'=>$post] );
    }
}
