<?php

return [
    'title' => 'MOVILIDAD',
    'subtitle' => 'Adaptación al entorno',
    'body' => 'La estructura y organización de HUMANA COM & TRAD le permiten una gran agilidad y movilidad geográfica. En Brasil, la empresa ha atendido la demanda de servicios de interpretación en distintas ubicaciones, algunas muy lejanas de su sede en Belém do Pará, como han sido las ciudades de Macapá, en el extremo norte, Porto Velho, en el oeste amazónico, Brasilia, Porto Alegre, en el sur, o Maceió, en el este. De esa forma se puede afirmar que ha cubierto toda la geografía del país.'
];
