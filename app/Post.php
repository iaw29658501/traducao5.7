<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];
    protected $table = 'post';
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
