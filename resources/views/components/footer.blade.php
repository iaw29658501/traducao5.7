<footer class="page-footer font-small cyan darken-3 bg-light text-light footer-light p-2 mt-5">
    <!-- Footer Elements -->
     <a href="mailto:humana.com.trad@gmail.com"><h5 class="text-primary text-center pt-5" style="word-wrap: break-word">humana.com.trad@gmail.com</h5></a>
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3 ">
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
            <a href="https://traducaosimultaneabelem.com.br/"> © {{ now()->year . " " . __('copyright') }}</a>
        </div>
        <!-- Copyright -->
        @include('googletrack')
    </div>
</footer>
