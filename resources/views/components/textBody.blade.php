<div class="container">
    <h4 class="display-5 mt-3 font-weight-light">
        {!! $body !!}
    </h4>
</div>
