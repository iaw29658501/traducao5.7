<div class="container-sec">

    <img id="paralax" src="/imgs/nice/missao-paralax.png" class="img-fluid" alt="traduções ajudam as crianças">
    <hr>
    <div class="mission">
        <h3><b>{{ __('home.mission')  }} </b></h3>
        <h5>{{ __('home.missionText') }} </h5>
    </div>
</div>
<style>
    @media only screen and (min-width: 45px) and (max-width: 780px) {
        #paralax {
            margin-left: -100%;
            min-width: 200%;
        }
    }

    @media only screen and (min-width: 768px) {

        .container-sec {
            position: relative;
        }

        .mission {
            position: absolute;
            top: 10vh;
            left: 5px;
            max-height: 40vh;
            max-width: 32vw;
        }

        .mission h3 {
            font-size: 2.5em
        }

        .mission h5 {
            font-size: 1.6em
        }

    }

    @media only screen and (min-width: 980px) {
        .mission {
            top: 2vh;
        }

        .mission h3 {
            font-size: 2.7em
        }

        .mission h5 {
            font-size: 1.9em
        }

    }

    @media only screen and (min-width: 1200px) {
        .mission {
            top: 10vh;
        }

        .mission h3 {
            font-size: 2.9em
        }

        .mission h5 {
            font-size: 2.2em
        }

    }
</style>
