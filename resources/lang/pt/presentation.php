<?php

return [
    'title' => 'APRESENTAÇÃO',
    'subtitle' => 'A empresa e seu desenvolvimento',
    'body' => 'A HUMANA COM & TRAD é uma empresa prestadora de serviços linguísticos com sede em Belém do Pará, na Amazônia brasileira, que iniciou suas atividades em 2008. Seu fundador e diretor é Sandro Ruggeri Dulcet, tradutor e intérprete profissional desde 1995, ano em que começou a prestar serviços para vários clientes e empresas locais do setor.
    Numa década, a HUMANA COM & TRAD prestou mais de 300 serviços, sendo 233 trabalhos de interpretação (75%) e 92 de tradução. A empresa conta hoje com um portfólio de 263 clientes entre fixos e eventuais. Até o momento, tem contado com a colaboração de 39 tradutores, que realizam traduções e revisões, e 97 intérpretes, dos quais 27 são locais e os demais de diferentes cidades do Brasil e do Escudo das Guianas (Guiana francesa, Suriname e Guiana).
    No que se refere à distribuição geográfica, a Humana tem desenvolvido suas atividades por todo o Brasil e em algumas cidades do exterior. Já atuamos em Porto Alegre, Brasília, Maceió, Porto Velho e Macapá, assim como em cidades do Escudo das Guianas, como Caiena, Paramaribo ou Georgetown, e até na África, como Túnis ou Dakar.
    Quanto a traduções escritas, a Humana gerenciou neste período 120 serviços de tradução, com um volume aproximado de 1.800.000 palavras, principalmente entre os seguintes idiomas: português, inglês, espanhol, francês e italiano. Os temas mais recorrentes, tanto nos serviços de tradução quanto nos de interpretação, são questões meio ambientais, pesquisa científica (zoologia, antropologia, botânica), políticas sociais, manejo sustentável de florestas, desenvolvimento social e econômico sustentável, tecnologia da informação, etc. Ela tem colaborado em projetos de localização de software de empresas de tradução. Presta regularmente também serviços de tradução juramentada em todas as línguas e combinações de e para o português.'
];
