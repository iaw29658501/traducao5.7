<?php

return [
    'title' => 'CARTERA DE CLIENTES Y SERVICIOS',
    'subtitle' => ' ',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'En relación a la traducción, algunos de los principales clientes de HUMANA han sido: Instituto Floresta Tropical, Museo Paraense Emílio Goeldi, agencias de la ONU, UNICEF, PNUMA, PNUD, OIT, FAO, la Organización del Tratado de Cooperación Amazónica, el Fórum Social Mundial, la Fundación Konrad Adenauer, el Office National des Fôrets International, IBASE, Instituto Evandro Chagas, Universidade Federal do Pará, EMBRAPA, Instituto Tecnológico Vale y The Nature Conservancy.
    <br><br>
    De los 92 encargos recibidos cabe destacar aquellos que han sido publicados en formato impreso o como página web, o bien deben ser publicados en el futuro:
    <br><br>
    • Processos de formação de fronteiras no Alto Amazonas/Solimões: a história das relações interétnicas dos Tikuna,de la antropóloga Claudia López, del Departamento de Antropologíadel Museu Paraense Emílio Goeldi, publicada en 2015.
<br><br>
    • La página del proyecto PPBIO, del Departamento de Zoología del MuseuParaenseEmílioGoeldi.
<br><br>
    • Informes de uso interno para las agencias de la ONU (PNUD, UNICEF Angola, UNICEF Brasil).
<br><br>
    • Farmacognosia y los sentidos en dos pueblos amazónicos, del antropólogo Glenn Harvey Shepard Jr. del Departamento de Antropología del Museo Paraense Emílio Goeldi.'
];
