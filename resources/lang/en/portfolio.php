<?php

return [
    'title' => 'PORTFOLIO OF CUSTOMERS AND SERVICES ',
    'subtitle' => 'Our customers speak for us',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'Regarding written translation, some of HUMANA\'s main clients have been: Instituto Floresta Tropical, Museu Paraense Emílio Goeldi, UN agencies UNICEF, UNEP, UNDP, ILO, FAO, Amazon Cooperation Treaty Organization, World Social Forum, Konrad Adenauer Foundation, Office National des Fôrets International, IBASE, Evandro Chagas Institute, different colleges of the Federal University of Pará, EMBRAPA, Instituto Tecnológico Vale and The Nature Conservancy.
    <br><br>
    Of the 92 orders for translations HUMANA received, many have been published in printed format or as web pages, or in the press:
        <br><br>
        • Processos de formação de fronteiras no Alto Amazonas/Solimões: a história das relações interétnicas dos Tikuna (Frontier formation processes in the High Amazon /Solimões Rivers: the history of the interethnic relations of the Tikuna) by anthropologist Claudia López, from the Department of Anthropology of the Museu Paraense Emílio Goeldi, published in 2015;
        <br>
        • The site of the PPBio Project, of the Department of Zoology of the Emílio Goeldi Paraense Museum;
        <br>
        • Internal use reports for UN agencies (UNDP, UNICEF Angola and UNICEF Brazil);
        <br>
        • Farmacognosia y los sentidos en dos pueblos amazonicos, (Pharmacognosy and senses in two Amazonian villages), by anthropologist Glenn Harvey Shepard Jr. of the Department of Anthropology of the Emílio Goeldi Paraense Museum.'
];
