 // make navbar non tranparent after scroll
/* window.onload = function () {
    console.log('adding effects')
    $(document).ready(function () {
        $(".dropdown, .btn-group").hover(function () {
            var dropdownMenu = $(this).children(".dropdown-menu");
            if (dropdownMenu.is(":visible")) {
                dropdownMenu.parent().toggleClass("open");
            }
        });

    });


    if ($(window).width() < 960) {
        $('.navbar').addClass('bg-primarydark');
    } else {
        navbarSolidCheck();
        // Transition effect for navbar
        $(window).scroll(function () {
            // checks if window is scrolled more than 500px, adds/removes solid class
            navbarSolidCheck();
        });
    }
};

function navbarSolidCheck() {
    if ($(this).scrollTop() > 200) {
        $('.navbar').addClass('bg-primarydark');
    } else {
        $('.navbar').removeClass('bg-primarydark');
    }
}
 */

// This will make the company numbers increase when in view
var firsttime=true;
$(document).ready(function(){
    $(window).scroll(function(){
        if ($('.count').isOnScreen() && firsttime ) {
            console.log("on screen")
            // The element is visible, do something
            increase()
            firsttime=false;
        }
    });
});

function increase() {
    $('.count').each(function () {
        var $this = $(this);
        jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
          duration: 1000,
          easing: 'swing',
          step: function () {
            $this.text(Math.ceil(this.Counter));
          }
        });
    });
}

$.fn.isOnScreen = function(){
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};


// Navitem spacing in english
// The navbar translation in English got bigger then the rest so I need to reduce the space

if ($(window).width() >= 992) { $('.nav-link').slice(-2).css('margin-left','-41px') }

var lang = document.getElementsByTagName("html")[0].getAttribute("lang");

function textResize() {
    if (lang == "en") {
        let width = $(window).width();
        if (width < 1000) {
            $('.medium').css('font-size','0.92rem');
        } else if (width < 500) {
            $('.medium').css('font-size','0.05rem');
        } else {
            $('.medium').css('font-size','1.3rem');
        }
    } else if (lang == "es") {
        let width = $(window).width();
        if (width < 1000) {
            $('.medium').css('font-size','0.92rem');
        } else if (width < 500) {
            $('.medium').css('font-size','0.05rem');
        } else {
            $('.medium').css('font-size','1.3rem');
        }
    }
}
window.onload= function() {
    textResize()
}
$(window).resize() = function() {
    textResize()
}
