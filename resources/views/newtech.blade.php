@component('components.template')
@slot('image')
    /imgs/traducao-hoje.jpg
@endslot
@slot('logo')
    /imgs/tecnologias.png
@endslot
@slot('alt')
{{ __('newtech.alt')  }}
@endslot
@slot('title')
{{ __('newtech.title')  }}
@endslot
@slot('subtitle')
{{ __('newtech.subtitle')  }}

@endslot
@slot('body')
{!! __('newtech.body') !!}
@endslot
@endcomponent
