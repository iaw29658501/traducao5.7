@component('components.template')
@slot('image')
/imgs/nice/apresentacao.jpg
@endslot
@slot('logo')
/imgs/logos/translation.png
@endslot
@slot('alt')
{{ __('presentation.alt')  }}
@endslot
@slot('title')
{{ __('presentation.title')  }}
@endslot
@slot('subtitle')
{{ __('presentation.subtitle')  }}
@endslot
@slot('body')
{!! __('presentation.body')  !!}
@endslot
@endcomponent
