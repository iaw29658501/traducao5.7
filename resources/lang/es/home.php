<?php

return [
    'aboutus'=>'Sobre Nosotros',
    'translation' => 'Traducción',
    'translationGrammar' => '[substantivo, s.f.] ',
    'translationDef' => 'Transformar exactamente [sin ambigüedades] una palabra,frase o texto de un idioma a otro',
    'interpretation' => 'Interpretación',
    'interpretationGrammar' => '[substantivo, s. f.]',
    'interpretationDef' => 'Acción que consiste en establecer, de forma simultánea o consecutiva, comunicación verbal o no verbal entre dos entidades, personas o entre un hablante y su audiencia.',
    'mission' => 'MISIÓN',
    'missionText' => 'La misión de HUMANA COM & TRAD es ofrecer servicios de traducción e interpretación de alto nivel, con calidad y competencia, atrayendo, sirviendo y reteniendo a sus clientes.',
    'vision' => 'VISIÓN',
    'visionText' => 'Pretendemos continuar siendo una empresa líder y distinguida en la prestación de servicios de calidad, creadora de valor para quien en ella participa, comprometida su entorno social y ambiental, responsable y justa, con actividades sostenibles a lo largo del tiempo donde colaboradores, suministradores, clientes y usuarios se consideren parte integrante de una dinámica de comunicación e intercambio de experiencias enriquecedoras. Queremos seguir contribuyendo para la creación de un ambiente favorable de crecimiento profesional y personal de todos los que trabajan en el campo de la traducción en Brasil.',
    'values'=>'Valores',
    'valuesText'=>'Los valores que nos inspiran y guían la estrategia y forma de actuar de la empresa son la dedicación y el entusiasmo por el trabajo realizado, la organización, la clareza y cuidado así como el uso racional en el trato de los recursos humanos y materiales disponibles. Forman parte de los valores de HUMANA el incentivo constante de sus colaboradores para que desarrollen su potencial y crezcan personal y profesionalmente. En contrapartida, el alto grado de exigencia con relación a la actitud profesional, organización, puntualidad y entrega conforme a  plazos son fundamentales en el momento de valorar la contribución y confiabilidad, valores estos tan o más importantes que el servicio en sí.',
    'banner' => 'Traductores desde los idiomas más comunes hasta los minoritarios',
    'translators'=>'Traductores',
    'interpreters' => 'Intérpretes',
    'translatedWords' => 'Palabras traducidas',
    'tradandinterp' => 'Traducción & Interpretación',
    'humaninstitute' => 'Instituto Humana',
    'researchanddevelopment' => 'Investigación y Desarrollo'
];
