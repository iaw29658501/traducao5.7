<?php

return [
    'title' => 'MISSION',
    'subtitle' => 'What is the reason for the company?',
    'body' => 'The mission of HUMANA COM & TRAD is to offer high quality translation and interpreting services, with quality and competence, attracting, serving and earning client fidelity. To achieve this, the company follows standardized processes of service that are also personalized thus establishing a lasting commitment among its network components: Employees, Suppliers, Clients and Users.
    <br><br>The company serves a diverse audience of researchers, faculty and university students, staff and members of research institutions and knowledge dissemination faculty members, and NGOs. The main translation and interpretation service orders it receives are translations and reviews of scientific articles for publication, essays on various topics, technical manuals or information texts, as well as simultaneous interpreting services at international conferences, using electronic transmitter/receiver systems or consecutive interpretation and follow-up.
    <br><br>The provision of these services is understood not only from an economic perspective, as a contracted activity with date and time of delivery and previously agreed value between the parties: it also represents a field of relations that the company establishes with customers, suppliers and collaborators to create professional and human bonds of trust and mutual respect, development and growth with positive long-term developments. The business activity that HUMANA creates thus fosters an additional value where service delivery represents an agglutinative action where everyone participates contributing their knowledge and enthusiasm.',
    'title2'=>'VISION',
    'subtitle2'=> 'What do we plan the company to become for during the next five years?',
    'body2'=>'We wish to continue being a leading and differentiated company in the market of quality language-service providers, creating added value for those who directly or indirectly participate in it, committed to the social and ecological environment, responsible and fair, with sustainable activities over time, where Collaborators, Suppliers, Clients and Users feel to be an integral part of a dynamics of communication and exchange of enriching experiences. We wish to continue to contribute to the creation of a favorable environment for professional and personal growth of all those working in the field of translation and interpretation in Brazil and other countries.',
    'title3'=>'VALUES',
    'subtitle3'=> 'What do we believe in and what are we?',
    'body3'=>'The values ​​that inspire us and guide the company\'s strategy and action are dedication and enthusiasm at work, organization, transparency, care and rational use in the treatment of available human and material resources. HUMANA values ​​the constant encouragement of its employees to develop their potential and grow professionally and personally. On the other hand, the high degree of professional attitude, organization, schedules and defined deadlines is fundamental in the assessment of contribution and reliability, values ​​that are as important as the work on its own.',
];

