<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')
@include('components.nav')
<body>
    <br><br><br>
<div class="container text-center">
    <br><br>
    @if (Auth::check())
        <a href="/contactme" class="btn btn-primary mb-5">Mensagens de Clientes</a>
        <br>
        <a href="/post/create" class="btn btn-primary mb-5">Criar novo post</a>
        <br>
    @endif
    @foreach ($posts->all() as $post)
        <a href="/pt/blog/{{ $post->slug }}" class="h3">{{ $post->title }}</a>
        <h6>{!! substr(strip_tags($post->content),0,300) !!} {{ strlen(strip_tags($post->content)) > 300 ? "..." : "" }} </h6>
        <a href="/pt/blog/{{ $post->slug }}" class="btn btn-outline-primary">Ler Mais</a>
        <hr>
    @endforeach

</div>
@include('components.bootstrap')
@include('components.footer')
<link rel="stylesheet" href="/css/style.css">
</body>

</html>
