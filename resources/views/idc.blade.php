@component('components.template')
@slot('image')
/imgs/id-guama.jpg
@endslot
@slot('logo')
/imgs/pesquisa-desenvolvimento.png
@endslot
@slot('alt')
{{ __('situation.alt')  }}
@endslot
@slot('title')
{{ __('situation.title')  }}
@endslot
@slot('subtitle')
{{ __('situation.subtitle')  }}
@endslot
@slot('body')
{{ __('situation.body')  }}
@endslot
@endcomponent
