<?php

return [
    'title' => 'SITUAÇÃO COMUNICATIVA',
    'subtitle' => 'As especificidades dos eventos',
    'alt' => 'Serviço de tradução simultanea em todo Brasil',
    'body' => 'Para atender a esta demanda por serviços especializados, a HUMANA COM & TRAD analisa as condições específicas de cada evento para optar pelos meios humanos e técnicos mais adequados para melhor atender cada caso. Pelo fato de atuar numa região limítrofe, a companhia assume dificuldades adicionais tanto na logística como nos recursos humanos qualificados a disposição. Para superar estas condições especiais, a HUMANA tem contado, ao longo destes anos, com uma série de mecanismos de adaptação ao entorno, com material técnico especialmente concebido para viajar, de montagem fácil e rápida, e uma equipe humana altamente qualificada e disposta a trabalhar em condições extremas.
    Além disso, desde sua criação, a empresa tem organizado uma série de cursos de capacitação para jovens tradutores e intérpretes, realizados pelo Instituto HUMANA em colaboração com centros universitários locais e do resto do Brasil. Esse trabalho de formação tem sido fundamental para transformar um panorama de escassez de recursos capacitados em um ambiente propício, onde eventos de grande envergadura têm sido possíveis (Fórum Social Mundial, Idea 2010).'
];
