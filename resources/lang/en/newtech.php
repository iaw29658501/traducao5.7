<?php

return [
    'title' => 'TRANSLATION TODAY AND NEW TECHNOLOGIES',
    'subtitle' => 'Always updated',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'Since its beginnings in 2008, HUMANA has been collaborating in the software localization project of the Ukrainian company AIT, specializing in the development of specific programs for translation agencies and freelance translators. Used by agencies or freelance translators, programs such as PROJETEX 3D, AnyCount, TO3000 and AceProof are designed to optimize translation project management tasks as well as quality control of translations.
    <br><br>
    A Localization project embeds a particularly problematic service in dealing with large volumes of words and varied types of text to be translated, and they represent an effort by HUMANA to improve the management of it own projects.
    <br><br>
    They entail the implementation of a series of the latest IT resources and workflow management in terms of quality control of the final product, the use of specific translation software, networking and cloud-run project management during development. These modern instruments in our translation toolshed give support to the use of auxiliary tools for what has been called as Computer Aided Translation (CAT).
    <br><br>
    Thus, software localization is an example of the changes in the execution of project management tasks and translator teams in small and large tasks, where new translation technologies play a fundamental role that radically changes the way to work from the time of the paper dictionaries and the handwritten sheets. New technologies are viable nowadays thanks to advances in the field of Computer Science applied to Linguistics. This has allowed us to handle large amounts of text while creating databases (translation memories, glossaries and text corpus) and a final verification of the translation based on information technology.
    <br><br>
    The use of these technological tools represents high level of investment in terms of the time devoted to constant qualification and updating, with the consequent economic cost involved, as the industry around the TAC programs becomes more complex and, at the same time, more relevant for professionals. Albeit the costs, the use of these programs allows for a reduction of the timelines of service, fostering a faster production pace of large volumes of text that, in turn, allow gains in competitiveness in the market and reduction of costs, all of which  can be benefit the customer.
    <br><br>
    Regarding the processing of a translation service order, and to enable high quality translation performance, reference should be made to the specific standards defined for the different stages through which each order passes. The quality control processes employed by HUMANA described below give an idea of ​​the management tasks implied in them where the quality control is used in a transversal way and starts when the order is made and until the delivery of the service.'

];
