<div class="container numbers">
<div class="d-flex flex-column flex-md-row justify-content-around">
    <div class="text-center">
        <img class="m-5 little-right" src="/imgs/tradutores-home.png" alt="">
        <h1> <span class="count">39</span></h1> <h4> @lang('home.translators') </h4>
    </div>
    <div class="text-center">
        <img class="m-5"  src="/imgs/interpretes-home.png" alt="">
        <h1> <span class="count">97</span></h1> <h4> @lang('home.interpreters') </h4>
    </div>
    <div class="text-center">
        <img class="m-5" src="/imgs/palavras-home.png" alt="">
        <h1> <span class="count">2</span>M</h1> <h4> @lang('home.translatedWords')  </h4>
    </div>
</div>
</div>
