<!DOCTYPE html>
<html
    lang="{{ str_replace('_', '-', app()->getLocale()) === "pt" ? "pt-br" : str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')
@include('components.nav')

<body>
    @isset($image)
    <img src="{{ $image }}" alt="{{ isset($alt) ? $alt : '' }} " class="img-fluid ">
    @endisset
    <div class="container-fluid padding text-center p-4 pb-5">

        @isset($logo)
        <img class="m-5 max-width-40" src="{{ $logo }}" alt="Tradução simultânea">
        @endisset
        @isset($title)
        <h2 class="m-5 font-weight-bold template-text-color">{{ $title }}</h2>
        @endisset
        @isset($subtitle)
        <h2 class="display-5 m-5 template-text-color">{{ $subtitle }} </h2>
        @endisset
        @isset($body)
        <div class="container">
            <h4 class="display-5 mt-3 font-weight-light">
                {!! $body !!}
            </h4>
        </div>
        @endisset
        @isset($slot)
        {{ $slot }}
        @endisset


        {{--     Repeating text blocks    --}}

        @isset($title2)
    </div>
    <div style="background-color: #F2F5F8; width: 100vw" class="p-4 pb-5 m-2 padding text-center">
        <h2 class="m-5 font-weight-bold template-text-color">{{ $title2 }}</h2>
        @endisset
        @isset($subtitle2)
        <h2 class="display-5 m-5 template-text-color">{{ $subtitle2 }} </h2>
        @endisset
        @isset($body2)
        <div class="container">
            <h4 class="display-5 mt-3 font-weight-light">
                {!! $body2 !!}
            </h4>
        </div>
    </div>
    @endisset

    @isset($title3)
    <div class="container-fluid padding text-center p-4 pb-5 m-2">
        <h2 class="m-5 font-weight-bold template-text-color">{{ $title3 }}</h2>
        @endisset
        @isset($subtitle3)
        <h2 class="display-5 m-5 template-text-color">{{ $subtitle3 }} </h2>
        @endisset
        @isset($body3)
        <div class="container">
            <h4 class="display-5 mt-3 font-weight-light">
                {!! $body3 !!}
            </h4>
        </div>
    </div>

        @endisset

        {{-- Events map --}}
        @isset($map)
        <iframe class="col-12 col-md-9" src="https://www.google.com/maps/d/embed?mid=1ryOA66kPJhSAEgl0nUldOA3oFno"
            frameborder="0" style="border:0" width="640" height="480" style="height:400px; width: 70%"></iframe>
        @endisset

        @include('components.bootstrap')
        @include('components.footer')
    </div>
</body>
<link rel="stylesheet" href="/css/style.css">
@if(! isset($navdontchange))
<script src="/js/navColorAndDropDown.js"></script>
@endif

</html>
