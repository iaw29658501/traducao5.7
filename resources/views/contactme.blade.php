<!DOCTYPE html>
<html
    lang="{{ str_replace('_', '-', app()->getLocale()) === "pt" ? "pt-br" : str_replace('_', '-', app()->getLocale()) }}">
@include('components.head')
@include('components.nav')

<body>
    @isset($image)
    <img src="{{ $image }}" alt="{{ isset($alt) ? $alt : '' }} " class="img-fluid ">
    @endisset
    <div class="container-fluid padding text-center">
        <div class="container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Delete</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Data</th>
                        <th scope="col">Email</th>
                        <th scope="col">Assunto</th>
                        <th scope="col">Mensagem</th>
                    </tr>
                </thead>
                <tbody>
                    @isset($contactme)
                    @foreach($contactme as $contact)
                    <tr>
                        <td><a href="/contactme-del/{{ $contact->id }}" class="btn btn-danger">Delete</a></td>
                        <th scope="row">{{ $contact->name }}</th>
                        <td>{{ $contact->created_at }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->subject }}</td>
                        <td>{{ $contact->body }}</td>
                      </tr>

                    @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
</body>
@include('components.bootstrap')
@include('components.footer')
<link rel="stylesheet" href="/css/style.css">
@if(! isset($navdontchange))
<script src="/js/navColorAndDropDown.js"></script>
@endif

</html>
