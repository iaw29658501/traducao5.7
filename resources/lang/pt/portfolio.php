<?php

return [
    'title' => 'PORTFÓLIO DE CLIENTES E SERVIÇOS',
    'subtitle' => 'Com a palavra o usuário final',
    'alt' => 'Serviço de tradução simultanea em Belém',
    'body' => 'No que diz respeito à tradução, alguns dos principais clientes da HUMANA têm sido: Instituto Floresta Tropical, Museu Paraense Emílio Goeldi, agências da ONU, Unicef, Pnuma, Pnud, OIT, FAO, Organização do Tratado de Cooperação Amazônica, Fórum Social Mundial, Fundação Konrad Adenauer, Office National des Fôrets International, Ibase, Instituto Evandro Chagas, diferentes faculdades da Universidade Federal do Pará, Embrapa, Instituto Tecnológico Vale e The Nature Conservancy.<br><br>Das 92 encomendas de traduções recebidas cabe destacar aquelas que foram publicadas em formato impresso ou como páginas web, ou que ainda se encontram no prelo: <br>• Processos de formação de fronteiras no Alto Amazonas/Solimões: a história das relações interétnicas dos Tikuna da antropóloga Claudia López, do departamento de Antropologia do Museu Paraense Emílio Goeldi, publicada em 2015 <br>• O site do Projeto PPBio, do Departamento de Zoologia do Museu Paraense Emílio Goeldi; <br>• Relatórios do projeto REDD+ para o Escudo das Guianas (em inglês, francês, português e neerlandês), publicados aquí<br>• Relatórios de uso interno para agências da ONU (PNUD, UNICEF Angola e UNICEF Brasil);<br>• Farmacognosia y los sentidos en dos pueblos amazónicos, do antropólogo Glenn Harvey Shepard Jr. do Departamento de Antropologia do Museu Paraense Emílio Goeldi.'
];
