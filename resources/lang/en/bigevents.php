<?php

return [
    'title' => 'EMBLEMATIC EVENTS',
    'subtitle' => 'Our history endorses us',
    'body' => 'With regard to the events performed outside Brazil, it is important to highlight the activities of HUMANA in the Guiana Shield, where it participated in scientific congresses on the environment, such as the Congress on Social and Ecological Biodiversity, in his 2nd edition in Macapá, 3rd edition in Paramaribo and 4th in Georgetown. It was also hired by the Office National des Forêts during the implementation of the REDD + project for the Guiana Shield, which turned into a series of 14 technical meetings under the directives of the UN environment agenda where task forces formed by forest technicians and policy makers of the different countries discussed the creation of common protocols for data analysis, assessment of challenges and proposals for environmental policies.
    <br> <br>
    These activities, with itinerant meetings in the main cities of the region and also in places far from the capitals, stand out for the technical and logistical challenges they entail, especially the logistics of moving equipment and personnel throughout the territory of the Guyana Shield.
    <br> <br>
    Another equally relevant and complex group of events is composed of the meetings and seminars of the World Social Forum (WSF) held in Belém in 2009, as well as the congress of art educators, IDEA 2010. In the case of the WSF 2009, 12 activities were carried out in a wide variety of communicative situations with a varied audience, which dealt, among other matters, with religious themes (World Forum of Theology and Liberation), legal (World Forum of Judges), educational (World Forum of Local Authorities, World Forum of Via Campesina). In addition, in order to attend the ad hoc meetings of the World Social Forum working groups, the HUMANA COM & TRAD team traveled on three occasions to Africa: Dakar, in Senegal in 2011 and to Tunis, in Tunisia in 2013 and 2015.
    <br> <br>
    The IDEA 2010 art educators\' meeting represents a case of a particularly complex event organization, where HUMANA COM & TRAD offered viable and efficient logistical solutions promptly responding to the challenges presented: professional interpreters (25) and volunteers (70) who worked in the four official languages ​​of the event (Portuguese, Spanish, English and French), as well as technicians (3), receptionists (13) and simultaneous translation equipment (2,200 units). The meetings were held either in a central auditorium with adequate technical facilities, or in small, improvised classrooms for parallel meetings that took place in up to 18 venues at a time.'
];

