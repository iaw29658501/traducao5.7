<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @if(isset($title))
    <title>{{ $title }}</title>
    @else
		<title>Tradução Simultânea em Belém - Apresentação - Tradução Simultânea em Belém - Serviço de tradução em Belém-PA</title>
    @endif
    <meta name="keywords" content="tradução em belem, tradução simultanea em belem, serviço de tradução em belem, serviço de tradução simultanea em belem, tradutor em belem, tradutores em belem, tradutor profissional em belem, tradução de documentos em belem" />
	<meta name="description" content="Precisando de serviço de tradução em Belém? A HUMANA COM &amp; TRAD é uma empresa prestadora de serviços linguísticos presente na Amazônia e apta a oferecer seus serviços em todo o Brasil. Consulte-nos!" />
    <link href="/imgs/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <!-- Required meta tags -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- social icons -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
   <!-- language flags -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
    @isset($description)
        <meta name="description" content="{{ $description }}" />
    @endisset
    @include('googletrack')
</head>
