@component('components.template')
@slot('image')
/imgs/mobilidade-traducao-simultanea-belem.jpg
@endslot
@slot('logo')
/imgs/mobili.png
@endslot
@slot('alt')
Traduções por todo o pais, do norte ao sul, leste ao oeste
@endslot
@slot('title')
{{ __('mobility.title')  }}
@endslot
@slot('subtitle')
{{ __('mobility.subtitle')  }}
@endslot
@slot('body')
{{ __('mobility.body')  }}
@endslot
@endcomponent
